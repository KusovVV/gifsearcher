package com.gmail.victorkusov.gifsearcher.utils.enums;

public enum SourceType {

    PREVIEW("original_still"), ANIMATED("original");


    private final String stringValue;

    SourceType(final String s) {
        stringValue = s;
    }

    public String toString() {
        return stringValue;
    }
}
