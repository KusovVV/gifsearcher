package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class Gif {

    @SerializedName("type")
    private String type;
    @SerializedName("id")
    private String id;
    @SerializedName("slug")
    private String slug;
    @SerializedName("url")
    private String url;
    @SerializedName("bitly_gif_url")
    private String bitlyGifUrl;
    @SerializedName("bitly_url")
    private String bitlyUrl;
    @SerializedName("embed_url")
    private String embedUrl;
    @SerializedName("username")
    private String username;
    @SerializedName("source")
    private String source;
    @SerializedName("rating")
    private String rating;
    @SerializedName("content_url")
    private String contentUrl;
    @SerializedName("source_tld")
    private String sourceTld;
    @SerializedName("source_post_url")
    private String sourcePostUrl;
    @SerializedName("is_sticker")
    private int isSticker;
    @SerializedName("import_datetime")
    private String importDatetime;
    @SerializedName("trending_datetime")
    private String trendingDatetime;
    @SerializedName("user")
    private User user;
    @SerializedName("images")
    private HashMap<String,ImageInfo> images;
    @SerializedName("title")
    private String title;

    private boolean animated = false;

    public boolean isAnimated() {
        return animated;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getSlug() {
        return slug;
    }

    public String getUrl() {
        return url;
    }

    public String getBitlyGifUrl() {
        return bitlyGifUrl;
    }

    public String getBitlyUrl() {
        return bitlyUrl;
    }

    public String getEmbedUrl() {
        return embedUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getSource() {
        return source;
    }

    public String getRating() {
        return rating;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public String getSourceTld() {
        return sourceTld;
    }

    public String getSourcePostUrl() {
        return sourcePostUrl;
    }

    public int getIsSticker() {
        return isSticker;
    }

    public String getImportDatetime() {
        return importDatetime;
    }

    public String getTrendingDatetime() {
        return trendingDatetime;
    }

    public User getUser() {
        return user;
    }

    public HashMap<String, ImageInfo> getImages() {
        return images;
    }

    public String getTitle() {
        return title;
    }

}
