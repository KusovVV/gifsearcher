package com.gmail.victorkusov.gifsearcher.utils.enums;

public enum QueryParameter {


    API_KEY("api_key"), LIMIT("limit");

    private final String stringValue;

    QueryParameter(final String s) {
        stringValue = s;
    }

    public String toString() {
        return stringValue;
    }
}
