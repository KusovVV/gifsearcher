package com.gmail.victorkusov.gifsearcher.mvp.view.adapter;

import com.arellomobile.mvp.MvpView;

public interface GifsAdapterView extends MvpView{
    void updateItems(int start, int count);

    void updateItem(int position);
}
