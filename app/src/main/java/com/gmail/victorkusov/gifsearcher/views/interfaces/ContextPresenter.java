package com.gmail.victorkusov.gifsearcher.views.interfaces;

import android.content.Context;

public interface ContextPresenter {

    Context getContext();
}
