package com.gmail.victorkusov.gifsearcher.rest.client;

import com.gmail.victorkusov.gifsearcher.rest.model.ResponseData;
import com.gmail.victorkusov.gifsearcher.utils.Const;

import java.util.HashMap;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Repository {


    public static Single<ResponseData> getGifs(String query, String offset) {

        HashMap<String,String> queryMap = new HashMap<>();
        queryMap.put("q",query);
        queryMap.put("offset",offset);

        return RestClient.getInstance().getData(queryMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
