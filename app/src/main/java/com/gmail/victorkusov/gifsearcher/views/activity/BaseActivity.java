package com.gmail.victorkusov.gifsearcher.views.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.gmail.victorkusov.gifsearcher.mvp.BaseMvpView;

import butterknife.ButterKnife;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseMvpView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ButterKnife.bind(this);
    }

    @LayoutRes
    protected abstract int getContentView();

    @Override
    public void showErrorToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
