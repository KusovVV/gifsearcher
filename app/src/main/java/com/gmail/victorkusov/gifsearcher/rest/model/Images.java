package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

class Images {

    @SerializedName("fixed_height_still")
    public ImageInfo fixedHeightStill;
    @SerializedName("original_still")
    public ImageInfo originalStill;
    @SerializedName("fixed_width")
    public ImageInfo fixedWidth;
    @SerializedName("fixed_height_small_still")
    public ImageInfo fixedHeightSmallStill;
    @SerializedName("fixed_height_downsampled")
    public ImageInfo fixedHeightDownsampled;
    @SerializedName("preview")
    public ImageInfo preview;
    @SerializedName("fixed_height_small")
    public ImageInfo fixedHeightSmall;
    @SerializedName("downsized_still")
    public ImageInfo downsizedStill;
    @SerializedName("downsized")
    public ImageInfo downsized;
    @SerializedName("downsized_large")
    public ImageInfo downsizedLarge;
    @SerializedName("fixed_width_small_still")
    public ImageInfo fixedWidthSmallStill;
    @SerializedName("preview_webp")
    public ImageInfo previewWebp;
    @SerializedName("fixed_width_still")
    public ImageInfo fixedWidthStill;
    @SerializedName("fixed_width_small")
    public ImageInfo fixedWidthSmall;
    @SerializedName("downsized_small")
    public ImageInfo downsizedSmall;
    @SerializedName("fixed_width_downsampled")
    public ImageInfo fixedWidthDownsampled;
    @SerializedName("downsized_medium")
    public ImageInfo downsizedMedium;
    @SerializedName("original")
    public ImageInfo original;
    @SerializedName("fixed_height")
    public ImageInfo fixedHeight;
    @SerializedName("looping")
    public ImageInfo looping;
    @SerializedName("original_mp4")
    public ImageInfo originalVideo;
    @SerializedName("preview_gif")
    public ImageInfo previewGif;
}
