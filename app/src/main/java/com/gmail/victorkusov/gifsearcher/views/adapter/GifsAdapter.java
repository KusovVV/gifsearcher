package com.gmail.victorkusov.gifsearcher.views.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arellomobile.mvp.MvpDelegate;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gmail.victorkusov.gifsearcher.R;
import com.gmail.victorkusov.gifsearcher.mvp.presenter.adapter.GifsPresenter;
import com.gmail.victorkusov.gifsearcher.mvp.view.adapter.GifsAdapterView;
import com.gmail.victorkusov.gifsearcher.mvp.view.adapter.MvpBaseAdapter;
import com.gmail.victorkusov.gifsearcher.rest.model.Gif;
import com.gmail.victorkusov.gifsearcher.views.interfaces.ContextPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GifsAdapter extends MvpBaseAdapter<GifsAdapter.Holder> implements GifsAdapterView {

    @InjectPresenter
    GifsPresenter mPresenter;

    private ContextPresenter mContextPresenter;


    public GifsAdapter(MvpDelegate parentDelegate, ContextPresenter contextPresenter) {
        super(parentDelegate);
        mContextPresenter = contextPresenter;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gif, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        Glide.with(mContextPresenter.getContext())
                .load(mPresenter.getGifUri(position))
                .apply(new RequestOptions()
                        .centerCrop())
                .into(holder.mImgGif);

        holder.mImgContainer.setTag(position);
        holder.mImgContainer.setOnClickListener(this::onItemClick);
    }

    private void onItemClick(View view) {
        mPresenter.changeContent((int)view.getTag());
    }

    @Override
    public int getItemCount() {
        return mPresenter.getDataSize();
    }

    public void updateData(List<Gif> data) {
        mPresenter.updateData(data);
    }


    public void clearData() {
        mPresenter.clearData();
    }

    @Override
    public void updateItems(int start, int count) {
        notifyItemRangeChanged(start, count);
    }

    @Override
    public void updateItem(int position) {
        notifyItemChanged(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_gif)
        ImageView mImgGif;
        @BindView(R.id.img_container)
        CardView mImgContainer;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
