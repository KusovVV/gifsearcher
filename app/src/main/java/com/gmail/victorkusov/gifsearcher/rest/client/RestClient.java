package com.gmail.victorkusov.gifsearcher.rest.client;

import android.support.annotation.NonNull;

import com.gmail.victorkusov.gifsearcher.rest.ApiInterface;
import com.gmail.victorkusov.gifsearcher.utils.Const;
import com.gmail.victorkusov.gifsearcher.utils.enums.QueryParameter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static ApiInterface instance;

    private RestClient() {
        instance = new Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getConverterFactory()))
                .addCallAdapterFactory(getFactory())
                .client(getClient())
                .build()
                .create(ApiInterface.class);
    }

    @NonNull
    private RxJava2CallAdapterFactory getFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @NonNull
    private Gson getConverterFactory() {
        return new GsonBuilder().setLenient().create();
    }

    public static ApiInterface getInstance() {
        if (instance == null) {
            new RestClient();
        }
        return instance;
    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .addInterceptor(getDefaultParamsInterceptor())
                .build();
    }

    private Interceptor getDefaultParamsInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();
                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter(QueryParameter.API_KEY.toString(), Const.API_KEY)
                        .addQueryParameter(QueryParameter.LIMIT.toString(), String.valueOf(Const.LIMIT))
                        .build();
                Request.Builder requestBuilder = original.newBuilder().url(url);
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };
    }


    private Interceptor getLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.HEADERS);
    }
}
