package com.gmail.victorkusov.gifsearcher.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

public abstract class BasePresenter<V extends MvpView> extends MvpPresenter<V> {
    }
