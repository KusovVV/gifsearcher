package com.gmail.victorkusov.gifsearcher.mvp.view.activity;

import com.gmail.victorkusov.gifsearcher.mvp.BaseMvpView;
import com.gmail.victorkusov.gifsearcher.rest.model.Gif;
import com.gmail.victorkusov.gifsearcher.rest.model.ResponseData;

import java.util.List;

public interface MainActivityView extends BaseMvpView {

    void updateAdapter(List<Gif> data);

    void saveQuery(String query);

    void checkSavedQuery();
}
