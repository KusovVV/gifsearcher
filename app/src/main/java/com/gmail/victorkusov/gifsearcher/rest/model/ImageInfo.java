package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

public class ImageInfo {
    @SerializedName("url")
    private String url;
    @SerializedName("width")
    private String width;
    @SerializedName("height")
    private String height;
    @SerializedName("size")
    private String size;
    @SerializedName("mp4")
    private String videoUrl;
    @SerializedName("mp4_size")
    private String videoSize;
    @SerializedName("webp")
    private String webp;
    @SerializedName("webp_size")
    private String webpSize;
    @SerializedName("frames")
    private String frames;
    @SerializedName("hash")
    private String hash;

    public String getUrl() {
        return url;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getSize() {
        return size;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public String getWebp() {
        return webp;
    }

    public String getWebpSize() {
        return webpSize;
    }

    public String getFrames() {
        return frames;
    }

    public String getHash() {
        return hash;
    }
}
