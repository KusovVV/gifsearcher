package com.gmail.victorkusov.gifsearcher.mvp.view.adapter;

import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpDelegate;

public abstract class MvpBaseAdapter<H extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<H> {

    private MvpDelegate<? extends MvpBaseAdapter> mMvpDelegate;
    private MvpDelegate<?> mParentDelegate;

    public MvpBaseAdapter(MvpDelegate<?> parentDelegate) {
        mParentDelegate = parentDelegate;
        getMvpDelegate().onCreate();
    }

    private MvpDelegate getMvpDelegate() {
        if (mMvpDelegate == null) {
            mMvpDelegate = new MvpDelegate<>(this);
            mMvpDelegate.setParentDelegate(mParentDelegate, String.valueOf(0));

        }
        return mMvpDelegate;
    }
}
