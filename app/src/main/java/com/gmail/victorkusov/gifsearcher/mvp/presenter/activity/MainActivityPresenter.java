package com.gmail.victorkusov.gifsearcher.mvp.presenter.activity;

import com.arellomobile.mvp.InjectViewState;
import com.gmail.victorkusov.gifsearcher.mvp.presenter.BasePresenter;
import com.gmail.victorkusov.gifsearcher.mvp.view.activity.MainActivityView;
import com.gmail.victorkusov.gifsearcher.rest.client.Repository;
import com.gmail.victorkusov.gifsearcher.rest.model.Gif;
import com.gmail.victorkusov.gifsearcher.rest.model.ResponseData;
import com.gmail.victorkusov.gifsearcher.utils.Const;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class MainActivityPresenter extends BasePresenter<MainActivityView> {

    private List<Gif> mData;
    private boolean mLoading;
    private String mQuery;
    private int mOffset;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().checkSavedQuery();
    }

    public void findGifs(String query) {
        if (mQuery != null && mQuery.equals(query)) {
            return;
        }
        clearOffset();
        getViewState().saveQuery(query);
        mQuery = query;
        getData();
    }

    private void getData() {
        mLoading = true;
        Repository.getGifs(mQuery, String.valueOf(mOffset)).subscribe(new SingleObserver<ResponseData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(ResponseData responseData) {
                mLoading = false;
                mData = responseData.getData();
                getViewState().updateAdapter(mData);
            }

            @Override
            public void onError(Throwable e) {
                mLoading = false;
                getViewState().showErrorToast(e.getMessage());
            }
        });
    }

    public int getDataSize() {
        return mData.size();
    }

    public boolean isLoading() {
        return mLoading;
    }

    public void downloadNext() {
        mOffset += Const.LIMIT;
        getData();
    }

    public void clearOffset() {
        mOffset = 0;
    }
}
