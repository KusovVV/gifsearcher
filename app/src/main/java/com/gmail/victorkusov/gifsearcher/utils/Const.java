package com.gmail.victorkusov.gifsearcher.utils;

public class Const {
    public static final String BASE_URL = "https://api.giphy.com/v1/";

    public static final String API_KEY = "KU9Lk6MlKwJIyD0arjxpA33maPX4mcJ4";
    public static final int LIMIT = 20;
    public static final String PREFERENCES_NAME = "preffs";
    public static final String QUERY_KEY = "query_key";
    public static final String EMPTY_STRING = "";

    public static int SPAN_COUNT_PORTRAIT = 2;
    public static int SPAN_COUNT_LANDSCAPE = 3;
}
