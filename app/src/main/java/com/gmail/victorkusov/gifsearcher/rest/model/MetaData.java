package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

public class MetaData {
    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("response_id")
    private String responseId;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getResponseId() {
        return responseId;
    }
}
