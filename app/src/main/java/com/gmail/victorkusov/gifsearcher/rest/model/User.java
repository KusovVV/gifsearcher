package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("banner_url")
    private String bannerUrl;
    @SerializedName("banner_image")
    private String bannerImage;
    @SerializedName("profile_url")
    private String profileUrl;
    @SerializedName("username")
    private String username;
    @SerializedName("display_name")
    private String displayName;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getDisplayName() {
        return displayName;
    }
}
