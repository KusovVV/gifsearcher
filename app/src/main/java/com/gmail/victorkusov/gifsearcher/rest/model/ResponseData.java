package com.gmail.victorkusov.gifsearcher.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseData {

    @SerializedName("data")
    private List<Gif> data;
    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("meta")
    private MetaData meta;

    public List<Gif> getData() {
        return data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public MetaData getMeta() {
        return meta;
    }
}
