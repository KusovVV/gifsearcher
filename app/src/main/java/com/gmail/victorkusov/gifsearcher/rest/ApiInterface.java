package com.gmail.victorkusov.gifsearcher.rest;

import com.gmail.victorkusov.gifsearcher.rest.model.ResponseData;

import java.util.HashMap;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("gifs/search")
    Single<ResponseData> getData(@QueryMap(encoded = true) HashMap<String,String> query);
}
