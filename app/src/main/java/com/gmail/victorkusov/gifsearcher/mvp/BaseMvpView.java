package com.gmail.victorkusov.gifsearcher.mvp;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView{
    void showErrorToast(String message);

}
