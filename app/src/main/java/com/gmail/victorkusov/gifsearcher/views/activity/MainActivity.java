package com.gmail.victorkusov.gifsearcher.views.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.gmail.victorkusov.gifsearcher.R;
import com.gmail.victorkusov.gifsearcher.mvp.presenter.activity.MainActivityPresenter;
import com.gmail.victorkusov.gifsearcher.mvp.view.activity.MainActivityView;
import com.gmail.victorkusov.gifsearcher.rest.model.Gif;
import com.gmail.victorkusov.gifsearcher.utils.Const;
import com.gmail.victorkusov.gifsearcher.views.adapter.GifsAdapter;
import com.gmail.victorkusov.gifsearcher.views.interfaces.ContextPresenter;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements MainActivityView, ContextPresenter {

    @BindView(R.id.gifs_list_container)
    RecyclerView mGifsListContainer;

    @InjectPresenter
    MainActivityPresenter mPresenter;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGifsListContainer.setLayoutManager(new GridLayoutManager(this,
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                        Const.SPAN_COUNT_PORTRAIT : Const.SPAN_COUNT_LANDSCAPE));
        mGifsListContainer.setAdapter(new GifsAdapter(getMvpDelegate(), this));
        mGifsListContainer.addOnScrollListener(getOnScrollListener());
    }


    private RecyclerView.OnScrollListener getOnScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy != 0 && dy > 0) {
                    int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount &&
                            firstVisibleItemPosition >= 0 &&
                            totalItemCount >= mPresenter.getDataSize() &&
                            !mPresenter.isLoading()) {
                        mPresenter.downloadNext();
                    }
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null && searchManager != null) {
            SearchView searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            GifsAdapter adapter = (GifsAdapter) mGifsListContainer.getAdapter();
            adapter.clearData();
            String query = intent.getStringExtra(SearchManager.QUERY);
            mPresenter.findGifs(query);
        }
    }

    @Override
    public void updateAdapter(List<Gif> data) {
        GifsAdapter adapter = (GifsAdapter) mGifsListContainer.getAdapter();
        adapter.updateData(data);
    }

    @Override
    public void saveQuery(String query) {
        getSharedPreferences(Const.PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(Const.QUERY_KEY, query)
                .apply();
    }

    @Override
    public void checkSavedQuery() {
        String savedQuery = getSharedPreferences(Const.PREFERENCES_NAME, Context.MODE_PRIVATE).getString(Const.QUERY_KEY, Const.EMPTY_STRING);
        if (!savedQuery.isEmpty()) {
            mPresenter.findGifs(savedQuery);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }
}
