package com.gmail.victorkusov.gifsearcher.mvp.presenter.adapter;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.gmail.victorkusov.gifsearcher.mvp.presenter.BasePresenter;
import com.gmail.victorkusov.gifsearcher.mvp.view.adapter.GifsAdapterView;
import com.gmail.victorkusov.gifsearcher.rest.model.Gif;
import com.gmail.victorkusov.gifsearcher.utils.enums.SourceType;

import java.util.List;

@InjectViewState
public class GifsPresenter extends BasePresenter<GifsAdapterView> {

    private List<Gif> mData;

    public int getDataSize() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    public List<Gif> getData() {
        return mData;
    }

    public void updateData(List<Gif> data) {
        int previousSize = 0;
        if (mData == null) {
            mData = data;
        } else {
            previousSize = mData.size();
            mData.addAll(data);
        }
        getViewState().updateItems(previousSize, data.size());
    }


    public void clearData() {
        if (mData != null && !mData.isEmpty()) {
            mData.clear();
        }
    }

    public Uri getGifUri(int position) {
        boolean animated = mData.get(position).isAnimated();
        return Uri.parse(mData.get(position)
                .getImages()
                .get(animated?SourceType.ANIMATED.toString():SourceType.PREVIEW.toString())
                .getUrl());
    }

    public void changeContent(int position) {
        Gif item = mData.get(position);
        item.setAnimated(!item.isAnimated());
        getViewState().updateItem(position);
    }
}
